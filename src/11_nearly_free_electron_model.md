```{python initialize=true}
import numpy as np
import plotly.offline as py
import plotly.graph_objs as go

pi = np.pi
```

# Tight binding and nearly free electrons
_(based on chapters 15–16 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - formulate a general way of computing the electron band structure - the **Bloch theorem**.
    - recall that in a periodic potential all electron states are Bloch waves.
    - derive the electron band structure when the interaction with the lattice is weak using the **Nearly free electron model**.

Let's summarize what we learned about electrons so far:

* Free electrons form a Fermi sea ([lecture 4](4_sommerfeld_model.md))
* Isolated atoms have discrete orbitals ([lecture 5](5_atoms_and_lcao.md))
* When orbitals hybridize we get *LCAO* or *tight-binding* band structures ([lecture 7](7_tight_binding.md))

The nearly free electron model (the topic of this lecture) helps to understand the relation between tight-binding and free electron models. It describes the properties of metals.

These different models can be organized as a function of the strength of the lattice potential $V(x)$:

![](figures/models.svg)

## Bloch theorem

> All Hamiltonian eigenstates in a crystal have the form
> $$ \psi_n(\mathbf{r}) = u_n(\mathbf{r})e^{i\mathbf{kr}} $$
> with $u_n(\mathbf{r})$ having the same periodicity as the lattice potential $V(\mathbf{r})$, and index $n$ labeling electron bands with energies $E_n(\mathbf{k})$.

In other words: any electron wave function in a crystal is a product of a periodic part that describes electron motion within a unit cell and a plane wave.

### Extra remarks

The wave function $u_n(\mathbf{r})e^{i\mathbf{kr}}$ is called a **Bloch wave**.

The $u_n(\mathbf{r})$ part is some unknown function. To calculate it we need to solve the Schrödinger equation. It is hard in general, but there are two limits when $U$ is "weak" and $U$ is "large" that provide us with most intuition.

If we change $\mathbf{k}$ by a reciprocal lattice vector $\mathbf{k} \rightarrow \mathbf{k} + h\mathbf{b}_1 + k\mathbf{b}_2 + l\mathbf{b}_3$, and we change $u_n(\mathbf{r}) \rightarrow u_n(\mathbf{r})\exp\left[-h\mathbf{b}_1 - k\mathbf{b}_2 - l\mathbf{b}_3\right]$ (also periodic!), we obtain the same wave function. Therefore energies of all bands $E_n(\mathbf{k})$ are periodic in reciprocal space with the periodicity of the reciprocal lattice.

Bloch theorem is extremely similar to the ansatz we used in [1D](7_tight_binding.md), and to the description of the [X-ray scattering](10_xray.md).

## Nearly free electron model

In free electron model $E = \hbar^2 k^2/2m$.

* There is only one band
* The band structure is not periodic in $k$-space
* In other words the Brillouin zone is infinite in momentum space

Within the **nearly free electron model** we want to start from the dispersion relation of the free electrons and consider the effect of introducing a weak lattice potential. The logic is very similar to getting optical and acoustic phonon branches by changing atom masses (and therefore reducing the size of the Brillouin zone).

In our situation:

![](figures/nearly_free_electron_bands.svg)

The band gaps open where two copies of the free electron dispersion cross.

### Avoided level crossing

*Remark: this is an important concept in quantum mechanics, based on the perturbation theory. You will only learn it later in QMIII, so we will need to postulate some important facts.*

Let's focus on the first crossing. The momentum near it is $k = \pi/a + \delta k$ and we have two copies of the original band structure coming together. One with $\psi_+ \propto e^{i\pi x/a}$, another with $\psi_- \propto e^{-i\pi x/a}$. Near the crossing the wave function is the linear superposition of $\psi_+$ and $\psi_-$: $\psi = \alpha \psi_+ + \beta \psi_-$. We actually used almost the same form of the wave function in LCAO, except instead of $\psi_\pm$ we used the orbitals $\phi_1$ and $\phi_2$ there.

Without the lattice potential we can approximate the Hamiltonian of these two states as follows:
$$H\begin{pmatrix}\alpha \\ \beta \end{pmatrix} =
\begin{pmatrix} E_0 + v \hbar \delta k & 0 \\ 0 & E_0 - v \hbar \delta k\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta \end{pmatrix}.
$$

Here we used $\delta p = \hbar \delta k$, and we expanded the quadratic function into a linear plus a small correction.  

??? question "calculate $E_0$ and the velocity $v$"
    The edge of the Brilloin zone has $k = \pi/a$. Substituting this in the free electron dispersion $E = \hbar^2 k^2/2m$ we get $E_0 = \hbar^2 \pi^2/2 m a^2$, and $v=\hbar k/m=\hbar \pi/ma$.

Without $V(x)$ the two wave functions $\psi_+$ and $\psi_-$ are independent since they have a different momentum. When $V(x)$ is present, it may couple these two states.

So in presence of $V(x)$ the Hamiltonian becomes

$$
H\begin{pmatrix}\alpha \\ \beta \end{pmatrix} =
\begin{pmatrix} E_0 + v \hbar \delta k & W \\ W^* & E_0 - v \hbar \delta k\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta \end{pmatrix},
$$

Here the coupling strength $W = \langle \psi_+ | V(x) | \psi_- \rangle$ is the matrix element of the potential between two states. *(This where we need to apply the perturbation theory, and this is very similar to the LCAO Hamiltonian)*.

??? question "how does our solution satisfy the Bloch theorem? What is $u(x)$ in this case?"
    The wave function has a form $\psi(x) = \alpha \exp[ikx] + \beta \exp[i(k - 2\pi/a)x]$
    (here $k = \pi/a + \delta k$). Choosing $u(x) = \alpha + \beta \exp(2\pi i x/a)$ we see
    that $\psi(x) = u(x) \exp(ikx)$.

#### Dispersion relation near the avoided level crossing

We need to diagonalize a 2x2 matrix Hamiltonian. The answer is
$$ E(\delta k) = E_0 \pm \sqrt{v^2\hbar^2\delta k^2 + |W|^2}$$

Check out section 15.1.1 of the book for the details of this calculation.

#### Physical meaning of $W$

Now we expand the definition of $W$:

$$W = \langle \psi_+ | V(x) | \psi_- \rangle = \frac{1}{a}\int_0^{a} dx \left[e^{i\pi x/a}\right]^* V(x) \left[e^{-i\pi x/a}\right] = \frac{1}{a}\int_0^a e^{-2\pi i x /a} V(x) dx = V_1$$

Here $V_1$ is the first Fourier component of $V(x)$ (using a complex Fourier transform).

$$ V(x) = \sum_{n=-\infty}^{\infty} V_n e^{2\pi i n x/a}$$

#### Crossings between the higher bands

Everything we did applies to the crossings at higher energies, only there we would get higher Fourier components of $V(x)$: $V_2$ for the crossing between the second and third band, $V_3$ for the crossing between third and 4th, etc.

### Repeated vs reduced vs extended Brillouin zone

All different ways to **plot** the same dispersion relation (no difference in physical information).

Repeated BZ (all possible Bloch bands):

![](figures/nearly_free_electron_bands.svg)

* Contains redundant information
* May be easier to count/follow the bands

Reduced BZ (all bands within 1st BZ):

![](figures/reduced_nearly_free_electron_bands.svg)

* No redundant information
* Hard to relate to original dispersion

Extended BZ (n-th band within n-th BZ):

![](figures/extended_nearly_free_electron_bands.svg)

* No redundant information
* Easy to relate to free electron model
* Contains discontinuities

## Exercises
#### Exercise 1: Bloch's theorem
Suppose we have a crystal with lattice vectors $\mathbf{a}_ 1$, $\mathbf{a}_ 2$, and $\mathbf{a}_ 3$.

1. What can be said about the symmetry of the Hamiltonian $\hat{H}$ of this crystal?
2. Now define the translation operator $\hat{T}_{\alpha,\beta,\gamma}$ so that $$\hat{T}_{\alpha,\beta,\gamma} \psi(\mathbf{r}) = \psi(\mathbf{r} - \alpha \mathbf{a}_1 - \beta \mathbf{a}_2 - \gamma \mathbf{a}_3),$$ where $\alpha$, $\beta$, $\gamma$ are integers. Show that $\hat{T}_{\alpha,\beta,\gamma}$ and $\hat{H}$ commute.
3. Show that the Bloch wavefunctions defined in the lecture notes are eigenfunctions of $\hat{T}_{\alpha,\beta,\gamma}$. What are the corresponding eigenvalues? What does this say about the eigenfunctions of $\hat{H}$.
4. By applying $\hat{H}$ to the Bloch wavefunction, show that the Schrödinger equation can be rewritten as $$\left[ \frac{\mathbf{\hat{p}}^2}{2m} + \frac{\hbar}{m} \mathbf{k} \cdot \mathbf{\hat{p}} + \frac{\hbar^2 \mathbf{k}^2}{2m} + V(\mathbf{r}) \right] u_{n,\mathbf{k}}(\mathbf{r}) = E_{n,\mathbf{k}} u_{n,\mathbf{k}}(\mathbf{r}),$$ where $\mathbf{\hat{p}} =-i\hbar\nabla$.
5. What is $u_{n,\mathbf{k}}(\mathbf{r})$ in case of free electrons? Is your answer consistent with the equation above?

#### Exercise 2: the central equation in 1D
Let's consider a 1D crystal with a period $a$. Let $k_0$ be any wave number of an electron in the first Brillouin zone.

1. What $k_n$ are equivalent to $k_0$ in this crystal?
2. First, we assume that the electrons with these $k_n$ are free. In that case, what are the wavefunctions $\phi_n(x)$ and energies $E_n$ of these electrons?
3. Make a sketch of the dispersion relation using a repeated Brillouin zone representation. Indicate some $k_n$ and $E_n$ as well as the first Brillouin zone in your sketch.

    We will now introduce a weak periodic potential $V(x) = V(x+na)$ in our system. This causes coupling between eigenstates $\left| \phi_n\right>$ in the free electron case. In order to find the right eigenstates of the system with that potential, we need an 'LCAO-like' trial eigenstate given by $$\left|\psi\right> = \sum_{n=-\infty}^{\infty}C_n \left|\phi_n\right>$$

4. Using the trial eigenstate above and the Schrödinger equation, show that $$E C_m = \varepsilon_m C_m+\sum_{n=-\infty}^{\infty} V_{n}C_{m-n},$$ where $V_n$ are the Fourier components of the potential defined [above](#physical-meaning-of-w). Find an expression for $\varepsilon_m$. _**NB:** This equation is also known as the central equation (in 1D)._

    ??? hint
        - Apply $\left<\phi_m\right|$ to the Schrödinger equation.
        - To evaluate $\left<\phi_m\right| \hat{H} \left| \phi_n\right>$, it may be helpful to separate the kinetic energy and potential energy of the Hamiltonian.

5. Why is the dispersion relation only affected near $k=0$ and at the edge of the Brillouin zone (see also figures [above](#repeated-vs-reduced-vs-extended-brillouin-zone))? 

    ??? hint
        To answer this question, only consider consider two free electron wavefunctions in the Hamiltonian and ignore all the others. Between what two of free electron wavefunctions does the coupling give significant contribution to the energy levels of the free electron wavefunctions?

#### Exercise 3: the tight binding model vs. the nearly free electron model
Consider a 1D crystal with a periodic potential given by delta peaks: $$V(x) = -\lambda \sum_{n=-\infty}^{\infty} \delta(x+na),$$ where $\lambda>0$. In this exercise, we will find the band structure of this crystal in two ways:

- By means of the nearly free electron model explained in this lecture.
- By means of the tight binding model explained in [lecture 7](/7_tight_binding).

<!-- A comment to separate two lists -->

1. We first find the band structure using the nearly free electron model. To this end, we consider the effect of the potential on the free electron wavefunctions given by $\psi_1(x) \propto e^{ikx}$ and $\psi_2(x) \propto e^{i[k-2\pi/a]x}$ on the interval $k=[0,\pi/a]$. Derive a dispersion relation of the lower band using the Schödinger equation and the trial eigenstate $$\Psi(x) = \alpha \psi_1(x) + \beta \psi_2(x).$$

    ??? hint
        Using the Schrödinger equation and the trial eigenstate, first derive a 2×2 eigenvalue problem given by $$E \begin{pmatrix}\alpha \\ \beta\end{pmatrix} = \begin{pmatrix}\varepsilon_0(k)+V_0 & V_1^* \\ V_1 & \varepsilon_0(k - 2\pi/a) + V_0\end{pmatrix} \begin{pmatrix}\alpha \\ \beta\end{pmatrix}.$$ What are $\varepsilon_0(k)$, $V_0$ and $V_1$?

2. Make a sketch of the lower band.
3. We now use the tight binding model, where we know that the dispersion relation can be described by $$E = \varepsilon_0 - 2 t \cos (ka).$$ Find an expression for $\varepsilon_0=\left<n\right| \hat{H} \left|n\right>$ and $-t=\left<n-1\right| \hat{H} \left| n \right>$, where $\left<x|n\right>$ represent the wavefunction of a single delta peak well at site $n$. You may make use of the results obtained in [exercise 2 of lecture 5](/5_atoms_and_lcao/#exercise-2-application-of-the-lcao-model) or [look up the wavefunction](https://en.wikipedia.org/wiki/Delta_potential).
4. Compare the bands obtained in exercise 1 and 2: what are the minima and bandwidths (difference between maximum and minimum) of those bands?
5. For what $a$ and $\lambda$ is the nearly free electron model more accurate? And for what $a$ and $\lambda$ is the tight binding model more accurate?