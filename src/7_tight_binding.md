```{python initialize=true}
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Electrons and phonons in 1D
_(based on chapters 9.1-9.3 & 11.1-11.3 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - formulate equations of motion for electrons and phonons in 1D.
    - solve these equations to arrive at the corresponding dispersion relations.
    - derive the group velocity, effective mass, and density of states from the dispersion relation.

Last lecture:

* Vibrational modes of few-atom chains (analyzed using Newton's equations)
* Orbital energies of electrons in few-atom chains (analyzed using LCAOs)

This lecture:

* Phonons and electrons in chains of infinitely many atoms.
* Main idea: use *periodicity* in space, similar to periodicity in time

## Equations of motion

### Phonons

![](figures/phonons2.svg)

We start by considering a 1D chain of atoms. We assume that the atoms interact with their neighbours via a harmonic potential (as in lecture 6 - Bonds and spectra). I.e., we model the inter-atomic coupling by simple linear springs.

If atom $n$ has displacement $u_n$, then Newton's law gives us the equation of motion:

$$
m \ddot{u}_n = -\kappa (u_n - u_{n-1}) -\kappa (u_n - u_{n+1}).
$$

For a system of size $L = Na$ with periodic boundary conditions, we also have $u_N = u_0$.

### Electrons

![](figures/lattice_potential.svg)

Formulating the equation of motion for electrons is very similar: we consider a LCAO wave function $|\Psi \rangle = \sum_n \phi_n |n \rangle$ and assume a nearest-neighbour hopping $-t$ and on-site energy $E_0$. We thus need to solve the Schrödinger equation

$$
E \phi_n = E_0 \phi_n - t \phi_{n+1} - t \phi_{n-1}.
$$

The periodic boundary conditions imply $\phi_N = \phi_0$.

### Key idea for solving these equations

Our task is to find all normal modes and to take $L → ∞$.

All atoms are similar $⇒$ the solution should be similar for all atoms. We therefore attempt the following solution (ansatz)
for phonons:
$$u_n = Ae^{i \omega t - i k x_n},$$
and for electrons:
$$\phi_n = Be^{i E t/\hbar - i k x_n},$$
where $x_n=na$ and where we wrote the time-dependent solution of the Schrödinger equation to emphasize the similarity between the two systems.

As usual, periodicity quantizes k-space by requiring

$$e^{ikx_n} = e^{ik(x_n+L)} \quad ⇒ \quad k = p \frac{2\pi}{L} $$

with $p\in \mathbb{Z}$. As such, $e^{ikx_n} = e^{i p \frac{2\pi}{L} n a} = e^{i \frac{2 \pi n p}{N}}$ and we see that changing $p→p+N$ corresponds to exactly the same solution. Therefore, we have $N$ different solutions in total. Furthermore, solutions with $k$-values that differ by an integer multiple of $N\frac{2\pi}{L} = \frac{2\pi}{a}$ are identical (see figure).

```python
x = np.linspace(-.2, 2.8, 500)
fig, ax = pyplot.subplots()
ax.plot(x, np.cos(pi*(x - .3)), label=r'$k=\pi/a$')
ax.plot(x, np.cos(3*pi*(x - .3)), label=r'$k=3\pi/a$')
ax.plot(x, np.cos(5*pi*(x - .3)), label=r'$k=5\pi/a$')
sites = np.arange(3) + .3
ax.scatter(sites, np.cos(pi*(sites - .3)), c='k', s=64, zorder=5)
ax.set_xlabel('$x$')
ax.set_ylabel('$u_n$')
ax.set_xlim((-.1, 3.2))
ax.set_ylim((-1.3, 1.3))
ax.legend(loc='lower right')
draw_classic_axes(ax)
ax.annotate(s='', xy=(.3, -1.1), xytext=(1.3, -1.1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(.3 + .5, -1.25, '$a$', ha='center');
```

How many different solutions did we expect to find? We have a system with $N$ degrees of freedom (either $u_n$ or $\phi_n$), and therefore we expect $N$ normal modes (or eigenstates).

Because we proposed an ansatz with $N$ different plane-wave solutions, if we find an energy or frequency for each solution, we have fully solved the problem!

## Solving the equations of motion

### Phonons

First substitute the Ansatz into the equations of motion:
$$ -m \omega^2 A e^{i\omega t - ikx_n} = \kappa A  e^{i\omega t}(-2 e^{-ikx_n} + e^{-ikx_n+ika}+ e^{-ikx_n-ika}),$$
the exponents and $A$ drop out, and we get:
$$ -m \omega^2 = \kappa (-2 + e^{ika}+ e^{-ika})=\kappa [-2 + 2\cos(ka)],$$
or after a further simplification:
$$\omega = \sqrt{\frac{2\kappa}{m}}\sqrt{1-\cos(ka)} = 2\sqrt{\frac{\kappa}{m}}|\sin(ka/2)|$$,
where we used $1-\cos(x)=2\sin^2(x/2)$.

So we arrive at the dispersion relation

```python
k = np.linspace(-2*pi, 6*pi, 500)
fig, ax = pyplot.subplots()

pyplot.plot(k, np.abs(np.sin(k/2)))

ax.set_ylim(bottom=0, top=1.2)
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
pyplot.xticks(list(2*pi*np.arange(-1, 4)) + [-pi, pi],
              [r'$-2\pi$', '$0$', r'$2\pi$', r'$4\pi$', r'$6\pi$',
               r'$-\pi$', r'$\pi$'])
pyplot.yticks([1], [r'$2\sqrt{\frac{\kappa}{m}}$'])
pyplot.vlines([-pi, pi], 0, 1.1, linestyles='dashed')
pyplot.hlines([1], .1*pi, 1.3*pi, linestyles='dashed')
draw_classic_axes(ax)
ax.annotate(s='', xy=(-pi, -.15), xytext=(pi, -.15),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(0, -.25, '1st Brillouin zone', ha='center')
ax.set_ylim(bottom=-.7);
```
Here $k_p = 2\pi p / L$ for $0 ≤ p < N$ are *all the normal modes* of the crystal, and therefore we can rederive a better Debye model!

Before we had $\sum_p → \frac{L}{2\pi}\int_{-\omega_D/v_s}^{\omega_D/v_s}dk$, because we introduced the cutoff to fix the problem with the number of modes.

Now $\sum_p → \frac{L}{2\pi}\int_{-\pi/a}^{\pi/a}dk$, the integral is over a finite number of modes because *there is* only a finite number of modes.

**Sound velocity:** at small $k$, $\sin(ka/2)\approx ka/2$, and therefore $\omega \approx \sqrt{\kappa/m} k a$, so we have derived the existence of sound waves.

### Electrons

Substitute the Ansatz into the equations of motion:

$$
E e^{-ikx_n} = E_0 e^{-ikx_n} - t e^{-ikx_n-ika} - te^{-ikx_n+ika},
$$
and after canceling the exponents we immediately get
$$
E = E_0 - 2t\cos(ka),
$$
so we arrive at the dispersion relation:

```python
pyplot.figure()
k = np.linspace(-pi, pi, 300)
pyplot.plot(k, -np.cos(k))
pyplot.xlabel('$ka$'); pyplot.ylabel('$E$')
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', 0, r'$\pi$'])
pyplot.yticks([-1, 0, 1], ['$E_0-2t$', '$E_0$', '$E_0+2t$']);
```

We see that the electron dispersion consists of a 'band' of allowed energies. Such an energy band has $2N$ states ($N$ $k$-values and 2 spin values). If each atom contributes 2 electrons, all these states must be occupied. Therefore, applying an electric field cannot create an electric current $⇒$ we have explained insulators!

The band bottom is at $k=0$ (if $t>0$). We then approximate the energy near $k=0$:
$$
E = E_0 - 2t\cos{ka} \approx E_0 - 2t + t (ka)^2.
$$
Comparing this with $E=(\hbar k)^2/2m$, we see that the dispersion is similar to that of free electrons but with an effective mass given by $m^*=\hbar^2/2ta^2$.

In the following lectures we will see that an electron dispersion usually has multiple options for $E(k)$, each called an energy band. The complete dispersion relation is also called a *band structure*.


## Group velocity, effective mass, density of states

*(here we only discuss electrons; for phonons everything is the same except for replacing $E = \hbar \omega$)*

Question: what happens if we apply an external electric field to the crystal:

![](figures/electric_field.svg)

If electrons form bands, then in each band

$$ H = E(k) + \tilde{U}(r),$$

where $\tilde{U}(r) = -e|\mathbf{E}|r$ only includes slow variations of the electrostatic potential (the rapidly changing atomic potential is responsible for $E(k)$):

To derive expressions for the velocity and mass, we recall from Hamiltonian mechanics:

$$H = \frac{p^2}{2m} + U(r)$$

where Hamilton's equations give us the *velocity* $v$ and *force* $F$:

$$\begin{aligned}
v \equiv \frac{dr}{dt} &= \frac{\partial H(p, r)}{\partial p}\\
F \equiv \frac{dp}{dt} &= -\frac{\partial H(p, r)}{\partial r}.
\end{aligned}$$

The connection to quantum mechanics is made by $p = \hbar k$. Analogously, the group velocity for electrons in a band structure is given by $v \equiv \hbar^{-1}\partial E/\partial k$ is the **group velocity** (same as for phonons).

Similarly, the **effective mass** is defined by:

$$m_{eff} \equiv F\left(\frac{dv}{dt}\right)^{-1} $$.

Substituting, we get

$$m_{eff} = \hbar^2\left(\frac{d^2 E(k)}{dk^2}\right)^{-1}$$.

```python
pyplot.figure(figsize=(8, 5))
k = np.linspace(-pi, pi, 300)
meff = 1/np.cos(k)
color = list(matplotlib.rcParams['axes.prop_cycle'])[0]['color']
pyplot.plot(k[meff > 0], meff[meff > 0], c=color)
pyplot.plot(k[meff < 0], meff[meff < 0], c=color)
pyplot.ylim(-5, 5)
pyplot.xlabel('$ka$'); pyplot.ylabel('$m_{eff}$')
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', 0, r'$\pi$']);
```

### Density of states

The DOS is the number of states per unit energy. In 1D we have
$$g(E) = \frac{L}{2\pi}\sum |dk/dE| = \frac{L}{2\pi}\sum |v|^{-1},$$

The sum goes over all bands existing at a given energy, positive and negative momenta, and possible spin values.

Using

$$ E = E_0 - 2t \cos(ka), $$

we get

$$
ka = \pm\arccos[(E - E_0) / 2t],
$$
and
$$
|d k / d E| = \frac{1}{a}\frac{1}{\sqrt{4t^2 - (E - E_0)^2}}.
$$

You can get to this result immediately if you remember the derivative of arccosine. Otherwise you need to go the long way: compute $dE/dk$ as a function of $k$, express $k$ through $E$ as we did above, and take the inverse.

Also a sanity check: when the energy is close to the bottom of the band, $E = E_0 - 2t + \delta E$ we get $g(E) \propto \delta E^{-1/2}$, as we would expect in 1D.

## Summary

* By using plane waves in real space as an Ansatz we found all normal modes and eigenvectors
* Computing dispersion relations explains the problems we listed before (need for cutoff, lack of scattering with every single atom, existence of insulators).
* Electrons and phonons have a complicated nonlinear relation between momentum and velocity (**group velocity**), effective mass, and density of states

## Exercises

### Exercise 1: Lattice vibrations

1. What is a normal mode? What is a phonon? Why do phonons obey Bose statistics?
2. From the dispersion relation of a 1D monatomic chain given in the lecture notes, calculate the group velocity $v_g$ and density of states $g(\omega)$.
3. Sketch them.
4. From the 1D dispersion relation $\omega(k)$ in the picture below, sketch the group velocity $v_g(k)$ and the density of states $g(\omega)$.

![](figures/NNNdispersion.svg)

### Exercise 2: Vibrational heat capacity of a 1D monatomic chain

1. Give an integral expression for the total energy $U$ of a 1D monatomic chain (similarly to what was done within the Debye theory). To do so, first derive the density of states from the appropriate dispersion relation given in the lecture notes.
2. Give an integral expression for the heat capacity $C$. 
3. Compute the heat capacity numerically, using e.g. MatLab or Python.
4. Do the same for $C$ in the Debye model and compare the two. What differences do you see?

### Exercise 3: Next-nearest neighbors chain

Consider electrons in a 1D atomic chain again:

![](figures/lattice_potential.svg)

Let's expand the one-dimensional chain model by extending the range of the interaction further than the nearest neighbors:

$$ \langle \phi_n | H | \phi_{n+2}\rangle \equiv -t' ≠ 0.$$

1. Write down the new Schrödinger equation for this system.

??? hint

    There are now two more terms in the equation: $-t' \phi_{n-2} - t' \phi_{n+2}$.

2. Solve the Schrödinger equation to find the dispersion relation $E(k)$.

??? hint

    Use the same Ansatz as for the nearest neighbors case: $ \phi_n = \phi_0 \exp(ikna) $.

3. Calculate the effective mass $m^*$.
4. Sketch the effective mass as a function of $k$ for the cases $t=2t'$,$t=4t'$ and $t=10t'$.
