# Solutions for lecture 1 exercises

### Exercise 1: Heat capacity of a classical oscillator.

1.

$$
Z = \int_{-\infty}^{\infty}dp \int_{-\infty}^{\infty} dx e^{-\frac{\beta}{2m}p^2-\frac{\beta k}{2}x^2} = \sqrt{\frac{2\pi m}{\beta}}\sqrt{\frac{2\pi}{\beta k}} = \frac{2\pi}{\beta}\sqrt{\frac{m}{k}},
$$

where we used $\int_{-\infty}^{\infty}e^{-\alpha x^2} = \sqrt{\frac{\pi}{\alpha}}$.

2.

$$
\langle E \rangle = -\frac{1}{Z}\frac{\partial Z}{\partial \beta} = \frac{1}{\beta}
$$

3.

$$
C = \frac{\partial\langle E\rangle}{\partial T} = k_B
$$

4.

Since this is a 1D system, the prefactor is $1$ as expected.

### Exercise 2: Quantum harmonic oscillator.

1.

Take a look into the graph from the notes.

2.

$$
Z = \sum_{n = 0}^{\infty} e^{-\beta\hbar\omega(n + 1/2)} = e^{-\beta\hbar\omega/2}\frac{1}{1 - e^{-\beta\hbar\omega}} = \frac{1}{2\sinh(\beta\hbar\omega/2)},
$$

where we used $\sum_{n = 0}^{\infty}r^n = \frac{1}{1 - r}$.

3.

$$
\langle E\rangle = -\frac{1}{Z}\frac{\partial Z}{\partial\beta} = \frac{\hbar\omega}{2}\coth\frac{\beta\hbar\omega}{2} = \hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right) = \hbar\omega\left(n_B(\beta\hbar\omega) + \frac{1}{2}\right).
$$

4.

$$
C = \frac{\partial \langle E\rangle}{\partial T} = \frac{\partial\langle E\rangle}{\partial\beta}\frac{\partial\beta}{\partial T} = k_B(\beta\hbar\omega)^2\frac{e^{\beta\hbar\omega}}{(e^{\beta\hbar\omega} - 1)^2}.
$$

In the high temperature limit $\beta \rightarrow 0$ and $e^{\beta\hbar\omega} \approx 1 + \beta\hbar\omega$, so $C \rightarrow k_B$ which is the same result as in Exercise 1.1.

5.

We can see that for $\beta\hbar\omega = 1$ we already have $C \approx 0.92k_B$, so we can define a characteristic temperature $T_E = \hbar\omega/k_B$ such that temperatures above this one can be considered high.

6.

$$
\langle n\rangle = \frac{1}{Z}\sum_{n = 0}^{\infty} ne^{-\beta\hbar\omega(n + 1/2)} = 2\frac{e^{\beta\hbar\omega/2} - e^{-\beta\hbar\omega/2}}{2}e^{-\beta\hbar\omega/2}\frac{e^{-\beta\hbar\omega}}{(1 - e^{-\beta\hbar\omega})^2} = \frac{1}{e^{\beta\hbar\omega} - 1},
$$

where we used $\sum_{n = 0}^{\infty}nr^n = \frac{r}{(1 - r)^2}$.

### Exercise 3: Total heat capacity of a diatomic material.

1.

Use the formula $\omega = \sqrt{\frac{k}{m}}$.

2.

$E = N_{^6Li}\hbar\omega_{^6Li}(2 + 1/2)+N_{^7Li}\hbar\omega_{^7Li}(4 + 1/2)$.

3.

$E = N_{^6Li}\hbar\omega_{^6Li}\left(n_B(\beta\hbar\omega_{^6Li}) + \frac{1}{2}\right) + N_{^7Li}\hbar\omega_{^7Li}\left(n_B(\beta\hbar\omega_{^7Li}) + \frac{1}{2}\right)$.

4.

$C = \frac{N_{^6Li}}{N}C_{^6Li} + \frac{N_{^7Li}}{N}C_{^7Li}$ where the heat capacities are calculated with the formula from Excercise 2.4.
