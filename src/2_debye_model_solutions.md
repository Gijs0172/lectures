# Solutions for lecture 2 exercises

### Exercise 1: Debye model: concepts.

1, 2, 3. 

Look at the lecture notes.

4.

$$
g(\omega) = \frac{dN}{d\omega} = \frac{dN}{dk}\frac{dk}{d\omega} = \frac{1}{v}\frac{dN}{dk}.
$$

We assume that in $d$ dimensions there are $d$ polarizations.

For 1D we have that $N = \frac{L}{2\pi}\int dk$, hence $g(\omega) = \frac{L}{2\pi v}$.

For 2D we have that $N = 2\left(\frac{L}{2\pi}\right)^2\int d^2k = 2\left(\frac{L}{2\pi}\right)^2\int 2\pi kdk$, hence $g(\omega) = \frac{L^2\omega}{\pi v^2}$.

For 3D we have that $N = 3\left(\frac{L}{2\pi}\right)^3\int d^3k = 3\left(\frac{L}{2\pi}\right)^3\int 4\pi kdk$, hence $g(\omega) = \frac{3L^3\omega^2}{2\pi^2v^3}$.

###  Exercise 2: Debye model in 2D.

1.

Look at the lecture notes.

2.

$$
E = \int_{0}^{\omega_D}g(\omega)\hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right)d\omega = \frac{L^2}{\pi v^2\hbar^2\beta^3}\int_{0}^{\beta\hbar\omega_D}\frac{x^2}{e^{x} - 1}dx + T \text{ independent constant}.
$$

3.

High temperature implies $\beta \rightarrow 0$, hence $E = \frac{L^2}{\pi v^2\hbar^2\beta^3}\frac{(\beta\hbar\omega_D)^2}{2} + T \text{ independent constant}$, and then $C = \frac{k_BL^2\omega^2_D}{2\pi v^2} = 2Nk_B$. We've used the value for $\omega_D$ calculated from $2N = \int_{0}^{\omega_D}g(\omega)d\omega$.

4.

In the low temperature limit we have that $\beta \rightarrow \infty$, hence $E \approx \frac{L^2}{\pi v^2\hbar^2\beta^3}\int_{0}^{\infty}\frac{x^2}{e^{x} - 1}dx + T \text{ independent constant} = \frac{2\zeta(3)L^2}{\pi v^2\hbar^2\beta^3} + T \text{ independent constant}$. Finally $C = \frac{6\zeta(3)k^3_BL^2}{\pi v^2\hbar^2}T^2$. We used the fact that $\int_{0}^{\infty}\frac{x^2}{e^{x} - 1}dx = 2\zeta(3)$ where $\zeta$ is the Riemann zeta function.


###  Exercise 3: Different phonon modes.

1.

$$
g(\omega) = \sum_{\text{polarizations}}\frac{dN}{dk}\frac{dk}{d\omega} = \left(\frac{L}{2\pi}\right)^3\sum_{\text{polarizations}}4\pi k^2\frac{dk}{d\omega} = \frac{L^3}{2\pi^2}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)\omega^2
$$

$$
E = \int_{0}^{\omega_D}g(\omega)\hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right)d\omega = \frac{L^3}{2\pi^2\hbar^3\beta^4}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)\int_{0}^{\beta\hbar\omega_D}\frac{x^3}{e^{x} - 1}dx + T \text{ independent constant}.
$$

2.

Note that we can get $\omega_D$ from $3N = \int_{0}^{\omega_D}g(\omega)$ so everything cancels as usual and we are left with the Dulong-Petit law $C = 3Nk_B$.

3.

In the low temperature limit we have that $C \sim \frac{2\pi^2k_B^4L^3}{15\hbar^3}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)T^3$. We used that $\int_{0}^{\infty}\frac{x^3}{e^{x} - 1}dx = \frac{\pi^4}{15}$.

### Exercise 4: Anisotropic sound velocities.

$$
E = 3\left(\frac{L}{2\pi}\right)^3\int d^3k\hbar\omega(\mathbf{k})\left(n_B(\beta\hbar\omega(\mathbf{k})) + \frac{1}{2}\right) = 3\left(\frac{L}{2\pi}\right)^3\frac{1}{v_xv_yv_z}\int d^3\kappa\frac{\hbar\kappa}{e^{\beta\hbar\kappa} - 1} + T \text{ independent part},
$$

where we used the substitutions $\kappa_x = k_xv_x,\kappa_y = k_yv_y, \kappa_z = k_zv_z$. Finally

$$
E = \frac{3\hbar L^3}{2\pi^2}\frac{1}{v_xv_yv_z}\int_0^{\kappa_D} d\kappa\frac{\kappa^3}{e^{\beta\hbar\kappa} - 1} + T \text{ independent part} = \frac{3L^3}{2\pi^2\hbar^3\beta^4}\frac{1}{v_xv_yv_z}\int_0^{\beta\hbar\kappa_D} dx\frac{x^3}{e^{x} - 1} + T \text{ independent part},
$$

hence $C = \frac{\partial E}{\partial T} = \frac{6k_B^4L^3T^3}{\pi^2\hbar^3}\frac{1}{v_xv_yv_z}\int_0^{\beta\hbar\kappa_D} dx\frac{x^3}{e^{x} - 1}$. We see that the result is similar to the one with the linear dispersion, the only difference is the factor $1/v_xv_yv_z$ instead of $1/v^3$.
