_based on chapters 13.1-13.2 & 14.1-14.2 of the book_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Define the reciprocal space, and explain its relevance
    - Construct a reciprocal lattice from a given real space lattice
    - Compute the intensity of X-ray diffraction of a given crystal

### Reciprocal lattice - 1D recap

Recall that in [lecture 7](7_tight_binding.md) we discussed the reciprocal space (k-space) for a 1D lattice. We found that the points $k$ and $k+G$, where $G=m2\pi/a$ with $m$ an integer, are equivalent to eachother. The reason was that we considered waves of the form

$$
e^{ikx_n} = e^{ikna}
$$

with $n$ an integer, which remains unchanged if we shift $k\rightarrow k+G$:

$$
e^{i(k+G)na} = e^{ikna+im2\pi n} =  e^{ikna}
$$
where we used
$$
e^{iGx_n} = e^{im2\pi n} =  1.
$$

The points $G=m2\pi/a$ form the _reciprocal lattice_.

We will now generalize this discussion to describe the reciprocal lattice in 3 dimensions.

### Reciprocal lattice in 3D

For every real-space lattice

$$
\mathbf{R}=n_1\mathbf{a_1}+n_2\mathbf{a_2}+n_3\mathbf{a_3},
$$

where $n_1$, $n_2$ and $n_3$ are integers, there exists a reciprocal lattice

$$
\mathbf{G}=m_1\mathbf{b_1}+m_2\mathbf{b_2}+m_3\mathbf{b_3},
$$

where $m_1$, $m_2$ and $m_3$ are also integers.

Similar to the 1D case, the two lattices are related as follows:

$$
\mathrm{e}^{i\mathbf{G}\cdot\mathbf{R}}=1
$$

The above relation holds if

$$
\mathbf{a_i}\cdot\mathbf{b_j}=2\pi\delta_{ij}
$$

giving

$$
\mathrm{e}^{i\mathbf{G}\cdot\mathbf{R}}=\mathrm{e}^{2\pi i(n_1 m_1 + n_2 m_2 + n_3 m_3)}
$$

which is clearly 1.

### Example of a two-dimensional reciprocal lattice

![](figures/reciprocal_mod.svg)

Compose reciprocal lattice vectors.
Directions:

$\mathbf{a_1}\cdot\mathbf{b_2}=0\rightarrow\mathbf{a_1}\perp\mathbf{b_2}$

$\mathbf{a_2}\cdot\mathbf{b_1}=0\rightarrow\mathbf{a_2}\perp\mathbf{b_1}$

Lengths: $\mathbf{a_1}\cdot\mathbf{b_1}=\mathbf{a_2}\cdot\mathbf{b_2}=2\pi$

$|\mathbf{b_1}|=\frac{2\pi}{|\mathbf{a_1}|\cos 30^\circ}=\frac{4\pi}{|\mathbf{a_1}|\sqrt{3}}$, $|\mathbf{b_2}|=\frac{2\pi}{|\mathbf{a_2}|\cos 30^\circ}=\frac{4\pi}{|\mathbf{a_2}|\sqrt{3}}$

The reciprocal lattice vectors can be composed directly from their real-space counterparts:

$$
\mathbf{b_1}=\frac{2\pi(\mathbf{a_2}\times\mathbf{a_3})}{ \mathbf{a_1}\cdot(\mathbf{a_2}\times\mathbf{a_3})}
$$

$$
\mathbf{b_2}=\frac{2\pi(\mathbf{a_3}\times\mathbf{a_1})}{ \mathbf{a_1}\cdot(\mathbf{a_2}\times\mathbf{a_3})}
$$

$$
\mathbf{b_3}=\frac{2\pi(\mathbf{a_1}\times\mathbf{a_2})}{ \mathbf{a_1}\cdot(\mathbf{a_2}\times\mathbf{a_3})}
$$

Note that the denominator is the volume of the real-space unit cell spanned by $\mathbf{a_{1,2,3}}$,  These definitions are cyclic, because $\mathbf{a_1}\cdot(\mathbf{a_2}\times\mathbf{a_3})=\mathbf{a_2}\cdot(\mathbf{a_3}\times\mathbf{a_1})=\mathbf{a_3}\cdot(\mathbf{a_1}\times\mathbf{a_2})$.

The reciprocal lattice can also be described as a Fourier transform. We imagine the real-space lattice as a density function consisting of delta peaks, first in 1D:

$$
\rho(x)=\sum_{n} \delta(x-na)
$$

We take the Fourier transform of this function to find:

$$
{\mathcal F}_{k}\left[\rho(x)\right]=\int \mathrm{d}x\ \mathrm{e}^{ikx} \rho(x)=\sum_{n} \int \mathrm{d}x\ \mathrm{e}^{ikx} \delta(x-na)=\sum_{n} \mathrm{e}^{ikna}
$$

This sum is non-zero only if $k=2\pi m/a$, so when $k$ is in a reciprocal lattice point. Therefore, it can be rewritten as:

$$
{\mathcal F}_{k}\left[\rho(x)\right]=\frac{2\pi}{|a|}\sum_{m} \delta\left(k-\frac{2\pi m}{a}\right)
$$

The above can be generalized to three dimensions:

$$
{\mathcal F}_\mathbf{k}\left[\rho(\mathbf{r})\right]=\int \mathrm{d}\mathbf{r}\ \mathrm{e}^{i\mathbf{k}\cdot\mathbf{r}} \rho(\mathbf{r})
$$

### Periodicity of the reciprocal lattice
Any wave in a crystal with wave vector $\mathbf{k}$ can also be described with wave vector $\mathbf{k'}=\mathbf{k}+\mathbf{G}_0$, where $\mathbf{G}_0=h\mathbf{b_1}+k\mathbf{b_2}+l\mathbf{b_3}$.

A primitive unit cell of the reciprocal lattice contains a set of unique $\mathbf{k}$ vectors. Convention: _1st Brillouin zone_. All $\mathbf{k}$ vectors outside the 1st BZ have a copy inside the 1st BZ. So any wave process with arbitrary $\mathbf{k}$ can be described inside the 1st BZ.

![](figures/brillouin_mod.svg)

The 1st Brillouin zone = the Wigner Seitz cell of the reciprocal lattice.

### Diffraction
We will now discuss how incoming waves incident onto a crystal can be scattered by the lattice. These waves can be x-rays, neutrons or electrons.

We consider two atoms that are separated by a lattice vector $\mathbf{R}$, and an incoming wave with wave vector $\mathbf{k}$. While the atoms can scatter the incoming wave into all directions, we will find that only waves scattered in certain directions will interfere constructively and lead to a signal on the detector. These directions are directly related to the reciprocal lattice vectors.

To find these directions, let's consider a scattered wave with wave vector $\mathbf{k'}$. We assume only elastic scattering, meaning that $|\mathbf{k'}|=|\mathbf{k}|$. 

![](figures/scattering.svg)


A geometrical argument shows that the phase difference between the rays scattered off the two atoms is given by
$$
\Delta\phi = |\mathbf{k}|(\Delta x_1+\Delta x_2) = (\mathbf{k'}-\mathbf{k})\cdot\mathbf{R}
$$
The scattered waves will only interfere constructively if this phase difference equals an integer times $2\pi$. Considering the complete atomic lattice $\mathbf{R}$, the scattered wave amplitude is


$$
A\propto\sum_\mathbf{R}\mathrm{e}^{i\left((\mathbf{k'}-\mathbf{k})\cdot\mathbf{R}-\omega t\right)}
$$

This sum will only yield a finite value if:

$$
\mathbf{k'}-\mathbf{k}=\mathbf{G}
$$

In other words, if the difference between the outgoing and incoming wave vectors coïncides with a reciprocal lattice point. We can then expect constructive interference due to the lattice points. This requirement is known as the _Laue condition_. Note that the total intensity measured by the detector goes like $I\propto A^2$.

![](figures/laue_mod.svg)

In the above we have assumed that at each lattice point there is one single atom. But what if there are multiple atoms per unit cell? In that case each atom acquires a phase shift of its own and the total amplitude becomes:

$$
A\propto\sum_\mathbf{R}\mathrm{e}^{i\left(\mathbf{G}\cdot\mathbf{R}-\omega t\right)}\sum_j f_j\ \mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_j}
$$

where $f_j$ is a phenomenological form factor that may be different for atoms of different elements. The second sum is called the _structure factor_:

$$
S(\mathbf{G})=\sum_j f_j\ \mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_j}
$$

The structure factor can cause destructive interference due to the contents of the unit cell, even though the Laue condition is met. Examples:

- Example 1: Simplest case - 1 atom at each lattice point, $\mathbf{r}_1=(0,0,0)\rightarrow S=f_1$. In this case each reciprocal lattice point gives one interference peak, none of which is cancelled.
- Example 2: conventional cell of the FCC lattice.

![](figures/fcc_mod.svg)

Basis contains 4 identical atoms:

$$
\begin{aligned}
\mathbf{r}_1&=(0,0,0)\\
\mathbf{r}_2&=\frac{1}{2}(\mathbf{a}_1+\mathbf{a}_2)\\
\mathbf{r}_3&=\frac{1}{2}(\mathbf{a}_2+\mathbf{a}_3)\\
\mathbf{r}_4&=\frac{1}{2}(\mathbf{a}_3+\mathbf{a}_1)\\
f_1&=f_2=f_3=f_4\equiv f
\end{aligned}
$$

$$
\begin{aligned}
\mathbf{G}&=h\mathbf{b}_1+k\mathbf{b}_2+l\mathbf{b}_3\\
S&=f\left(\mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_1}+\mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_2}+\mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_3}+\mathrm{e}^{i\mathbf{G}\cdot\mathbf{r}_4}\right)\\
&=f\left(1+\mathrm{e}^{i/2(h\mathbf{b}_1\cdot\mathbf{a}_1+k\mathbf{b}_2\cdot\mathbf{a}_2)}+\mathrm{e}^{i/2(k\mathbf{b}_2\cdot\mathbf{a}_2+l\mathbf{b}_3\cdot\mathbf{a}_3)}+\mathrm{e}^{i/2(h\mathbf{b}_1\cdot\mathbf{a}_1+l\mathbf{b}_3\cdot\mathbf{a}_3)}\right)\\
&=f\left(1+\mathrm{e}^{i\pi(h+k)}+\mathrm{e}^{i\pi(k+l)}+\mathrm{e}^{i\pi(h+l)}\right)
\end{aligned}
$$

Parameters $h$, $k$, $l$ are integers $\rightarrow$ all terms are either $+1$ or $-1$ $\rightarrow$ puzzle. Solution:

$S=4f$ if $h$, $k$, $l$ are all even or all odd

$S=0$ in all other cases

## Summary
* We described how to construct a reciprocal lattice from a real-space lattice.
* Points in reciprocal space that differ by a reciprocal lattice vector are equivalent --> Band structure can be fully described by considering 1st Brillouin zone.
* Diffraction experiments reveal information about crystal structure.
* Laue condition: difference between wavevectors of incoming and diffracted waves matches a reciprocal lattice vector, necessary for constructive interference.
* Structure factor: describes the contribution of the atoms in a unit cell to the diffraction pattern.


## Exercises

### Exercise 1: Equivalence of direct and reciprocal lattice

The volume of a primitive cell of a lattice with lattice vectors $\mathbf{a}_1, \mathbf{a}_2, \mathbf{a}_3$ [equals](https://en.wikipedia.org/wiki/Parallelepiped#Volume) $V = |\mathbf{a}_1\cdot(\mathbf{a}_2\times\mathbf{a}_3)|$.

1. Find the volume of a primitive unit cell of the corresponding reciprocal lattice.
2. Derive the expressions for the lattice vectors $\mathbf{a}_i$ through the reciprocal lattice $\mathbf{b}_i$.

    ??? hint
        Make use of the vector identity
        $$\mathbf{A}\times(\mathbf{B}\times\mathbf{C}) = \mathbf{B}(\mathbf{A}\cdot\mathbf{C}) - \mathbf{C}(\mathbf{A}\cdot\mathbf{B})$$

3. Write down the primitive lattice vectors of the BCC lattice. Show that the reciprocal lattice of a BCC lattice is a FCC lattice and vice versa.
4. Determine the shape of the Brillouin zone of the FCC lattice.

### Exercise 2: Miller planes and reciprocal lattice vectors

Consider a family of Miller planes $(hkl)$ in a crystal.

1. Prove that the reciprocal lattice vector $\mathbf{G} = h \mathbf{b}_1 + k \mathbf{b}_2 + l \mathbf{b}_3$ is perpendicular to the Miller plane $(hkl)$.

    ??? hint
        Choose two vectors that lie within the Miller plane and are not parallel to each other; their vector product is perpendicular to that plane.

2. Show that the distance between two adjacent Miller planes $(hkl)$ of any lattice is  $d = 2\pi/|\mathbf{G}_\textrm{min}|$, where $\mathbf{G}_\textrm{min}$ is the shortest reciprocal lattice vector perpendicular to these Miller planes.
3. Find the family of Miller planes of the BCC lattice that has the highest density of lattice points. To solve this problem use that the density of lattice points per unit area on a Miller plane is $\rho = d/V$. Here $V$ is the volume of the primitive unit cell and $d$ is the distance between adjacent planes given in 2.2.

### Exercise 3: X-ray scattering in 2D

*(adapted from ex 14.1 and ex 14.3 of "The Oxford Solid State Basics" by S.Simon)*

Consider a two-dimensional crystal with a rectangular lattice and lattice vectors $\mathbf{a}_1 = (0.468, 0)$ nm and $\mathbf{a}_2 = (0, 0.342)$ nm (so that $\mathbf{a}_1$ points along $x$-axis and $\mathbf{a}_2$ points along $y$-axis).

1. Sketch the reciprocal lattice of this crystal.
2. Consider an X-ray diffraction experiment performed on this crystal using monochromatic X-rays with wavelength $0.166$ nm. By assuming elastic scattering, find the magnitude of the wave vectors of the incident and reflected X-ray beams.
3. On the reciprocal lattice sketched in 3.1, draw the "scattering triangle" corresponding to the diffraction from (210) planes. To do that use the Laue condition $\Delta \mathbf{k} = \mathbf{G}$ for the constructive interference of diffracted beams.

### Exercise 4: Structure factors

1. Compute the structure factor $\mathbf{S}$ of the BCC lattice.
2. Compute which diffraction peaks are missing.
2. How does this structure factor change if the atoms in the center of the conventional unit cell have a different form factor from the atoms at the corner of the conventional unit cell?
5. Explain why X-ray diffraction can be observed in first order from the (110) planes of a crystal with the BCC lattice, but not from the (110) planes of the FCC lattice.
