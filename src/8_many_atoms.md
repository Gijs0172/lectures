```{python initialize=true}
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

_(based on chapters 10-11 of the book)_  


!!! summary "Learning goals"

    After this lecture you will be able to:

    - formulate equations of motion for electrons or phonons in 1D, with multiple degrees of freedom per unit cell.
    - solve these equations to arrive at the dispersion relation.
    - derive the group velocity and density of states from the dispersion relation.
    - explain what happens with the band structure when the periodicity of the lattice is increased or reduced.

### More degrees of freedom per unit cell:

![](figures/phonons5.svg)

Before we used that all atoms are similar, but this doesn't work anymore. We need to generalize our ansatz. We label all degrees of freedom in each **unit cell** (a repeated element of the system).

For two atoms in a unit cell we have displacements $u_{1,n}$ and $u_{2,n}$, where the first index specifies the atom number within the unit cell and the second the unit cell number. Atom 1 has mass $m_1$ and atom 2 has mass $m_2$.

Having specified the degrees of freedom, let's write down the equations of motion:

$$
\begin{aligned}
m_1\ddot{u}_{1,n}=\kappa(u_{2,n}-2u_{1,n}+u_{2,n-1})\\
m_2\ddot{u}_{2,n}=\kappa(u_{1, n} - 2u_{2,n}+u_{1,n+1}),
\end{aligned}
$$

The new ansatz is conceptually the same as before: all unit cells should behave the same up to a phase factor:

$$
\begin{pmatrix}
u_{1,n}\\
u_{2,n}
\end{pmatrix} =
e^{i\omega t - ik na}
\begin{pmatrix}
A_{1}\\
A_{2}
\end{pmatrix}.
$$

Substituting this ansatz into the equations of motion we end up with an eigenvalue problem:
$$\omega^2
\begin{pmatrix}
m_1 & 0 \\ 0 & m_2
\end{pmatrix}
\begin{pmatrix}
A_{1} \\ A_{2}
\end{pmatrix} = \kappa
\begin{pmatrix}
2 & -1 - e^{ika} \\ -1-e^{-ika} & 2
\end{pmatrix}
\begin{pmatrix}
A_{1}\\ A_{2}
\end{pmatrix},$$
with eigenfrequencies
$$\omega^2=\frac{\kappa(m_1+m_2)}{m_1m_2}\pm \kappa\left\{\left(\frac{m_1+m_2}{m_1m_2}\right)^2-\frac{4}{m_1m_2}{\rm sin}^2\left(\frac{1}{2}ka\right)\right\}^{\frac{1}{2}}$$

Looking at the eigenvectors we see that for every $k$ there are now two values of $\omega$: one corresponding to in-phase motion (–) and anti-phase (+).

```python
def dispersion_2m(k, kappa=1, M=1.4, m=1, acoustic=True):
    Mm = M*m
    m_harm = (M + m) / Mm
    root = kappa * np.sqrt(m_harm**2 - 4*np.sin(k/2)**2 / Mm)
    if acoustic:
        root *= -1
    return np.sqrt(kappa*m_harm + root)

# TODO: Add panels with eigenvectors
k = np.linspace(-2*pi, 6*pi, 300)
fig, ax = pyplot.subplots()
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', '$0$', r'$\pi$'])
pyplot.yticks([], [])
pyplot.vlines([-pi, pi], 0, 2.2, linestyles='dashed')
pyplot.legend()
pyplot.xlim(-1.75*pi, 3.5*pi)
pyplot.ylim(bottom=0)
draw_classic_axes(ax, xlabeloffset=.2)
```

The lower branch is called _acoustic_ because its linear dispersion near $\omega=0$ matches the behavior of regular sound waves.
The upper branch is the _optical branch_ because it can cross with the (linear) dispersion relation of photons, allowing these phonons to efficiently emit and absorb photons.

As before, the phonon group velocity is given by $v={\rm d}\omega/{\rm d}k$.

The _density of states_ (DOS) is given by $g(\omega)=\frac{{\rm d}N}{{\rm d}\omega} = \frac{L}{2\pi} \sum \left| \frac{{\rm d}k}{{\rm d} \omega}\right|$, where the sum goes over all states at a given energy. In this case, the sum ensures we include the contribution to the DOS from both the positive and negative momenta.

We can graphically determine $\frac{ {\rm d} k}{{\rm d} \omega}$ from the dispersion relation $\omega(k)$:

```python
matplotlib.rcParams['font.size'] = 24
k = np.linspace(-.25*pi, 1.5*pi, 300)
k_dos = np.linspace(0, pi, 20)
fig, (ax, ax2) = pyplot.subplots(ncols=2, sharey=True, figsize=(10, 5))
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
ax.vlines(k_dos, 0, dispersion_2m(k_dos, acoustic=False),
          colors=(0.5, 0.5, 0.5, 0.5))
ax.hlines(
    np.hstack((dispersion_2m(k_dos, acoustic=False), dispersion_2m(k_dos))),
    np.hstack((k_dos, k_dos)),
    1.8*pi,
    colors=(0.5, 0.5, 0.5, 0.5)
)
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
ax.set_xticks([0, pi])
ax.set_xticklabels(['$0$', r'$\pi$'])
ax.set_yticks([], [])
ax.set_xlim(-pi/4, 2*pi)
ax.set_ylim((0, dispersion_2m(0, acoustic=False) + .2))
draw_classic_axes(ax, xlabeloffset=.2)

k = np.linspace(0, pi, 1000)
omegas = np.hstack((
    dispersion_2m(k, acoustic=False), dispersion_2m(k)
))
ax2.hist(omegas, orientation='horizontal', bins=75)
ax2.set_xlabel(r'$g(\omega)$')
ax2.set_ylabel(r'$\omega$')

# Truncate the singularity in the DOS
max_x = ax2.get_xlim()[1]
ax2.set_xlim((0, max_x/2))
draw_classic_axes(ax2, xlabeloffset=.1)
matplotlib.rcParams['font.size'] = 16
```

Note that $g(\omega)$ is generally plotted along the vertical axis and $\omega$ along the horizontal axis – the right plot above is just to demonstrate the relation between the dispersion and the DOS. The singularities in $g(\omega)$ at the bottom and top of each branch are called _van Hove singularities_.

### Consistency check with 1 atom per cell

But what if we take $m_1\rightarrow m_2$? Then we should reproduce the previous result with only one band!

The reason why we get two bands is because we have $a\rightarrow 2a$ compared to 1 atom per unit cell!

For $m_1\rightarrow m_2$, the dispersion diagram should come back to the diagram obtained for the first example (i.e. with identical masses).

To reconcile the two pictures, let's plot two unit cells in reciprocal space. (Definition: each of these unit cells is called a **Brillouin zone**, a concept that will come back later.)

```python
k = np.linspace(0, 2*pi, 300)
k_dos = np.linspace(0, pi, 20)
fig, ax = pyplot.subplots()
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
omega_max = dispersion_2m(0, acoustic=False)
ax.plot(k, omega_max * np.sin(k/4), label='equal masses')
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
ax.set_xticks([0, pi, 2*pi])
ax.set_xticklabels(['$0$', r'$\pi/2a$', r'$\pi/a$'])
ax.set_yticks([], [])
ax.set_xlim(-pi/8, 2*pi+.4)
ax.set_ylim((0, dispersion_2m(0, acoustic=False) + .2))
ax.legend(loc='lower right')
pyplot.vlines([pi, 2*pi], 0, 2.2, linestyles='dashed')
draw_classic_axes(ax, xlabeloffset=.2)
```

We see that doubling the lattice constant "folds" the band structure on itself, doubling all the bands.

## Summary

* By using plane waves in real space as an ansatz, we found all normal modes. (Just like in the case of 1 degree of freedom per unit cell).
* The density of states can be derived graphically from the dispersion relation.
* The dispersion relation of a system with period $a$ in real space is periodic with period $2\pi/a$ in $k$-space
* In a system with more than one degree of freedom per unit cell we need to consider independent amplitudes for each degree of freedom, and we get multiple bands.

## Exercises
#### Exercise 1: analyzing the diatomic vibrating chain
Recall the eigenfrequencies of a diatomic vibrating chain in the lecture notes with 2 different masses (can be found below [here](#more-degrees-of-freedom-per-unit-cell)).

1. Find the magnitude of the group velocity near $k=0$ for the _acoustic_ branch.

    ??? hint
        Make use of a Taylor expansion.

2. Show that the group velocity at $k=0$ for the _optical_ branch is zero.
3. Derive an expression for the density of states $g(\omega)$ for the _acoustic_ branch and small $ka$. Make use of your expression for the group velocity in 1.

#### Exercise 2: the Peierls transition
In the previous lecture, we have derived the electronic band structure of an 1D, equally spaced atomic chain. Such chains, however, are in fact not stable and the equal spacing will be distorted. This is also known as the [Peierls transition](https://en.wikipedia.org/wiki/Peierls_transition).

The spacing of the distorted chain alternates between two different distances and this also causes the hopping energy to alternate between $t_1$ and $t_2$. We further set the onsite energies of the atoms to $\epsilon$. The situation is depicted in the figure below.

![](figures/peierls_transition.svg)

Due to the alternating hopping energies, we must treat two consecutive atoms as two different orbitals ($\left| n,1 \right>$ and $\left| n,2 \right>$ in the figure) from the same unit cell. The corresponding LCAO of this chain is given by $$\left|\Psi \right> = \sum_n \left( \phi_n \left| n,1 \right> + \psi_n \left| n,2 \right>\right).$$ As usual, we assume that all these atomic orbitals are orthogonal to each other.

1. Indicate the length of the unit cell $a$ in the figure.
2. Using the Schrödinger equation, write the equations of motion of the electrons.

    ??? hint
        To this end, find expressions for $E \left< n,1 \vert \Psi \right> = \left< n,1 \right| H \left|\Psi \right>$ and $E \left< n,2 \vert \Psi \right> = \left< n,2 \right| H \left|\Psi \right>$.

3. Using the trial solutions $\phi_n = \phi_0 e^{ikna}$ and $\psi_n = \psi_0 e^{ikna}$, show that the Schödinger equation can be written in matrix form: $$\begin{pmatrix} \epsilon & t_1 + t_2 e^{-i k a} \\  t_1 + t_2 e^{i k a}  & \epsilon \end{pmatrix} \begin{pmatrix} \phi_0 \\ \psi_0 \end{pmatrix} = E \begin{pmatrix} \phi_0 \\ \psi_0 \end{pmatrix}.$$
4. Derive the dispersion relation of this Hamiltonian. Does it look like the figure of the band structure shown on the [Wikipedia page](https://en.wikipedia.org/wiki/Peierls_transition#/media/File:Peierls_instability_after.jpg)? Does it reduce to the 1D, equally spaced atomic chain if $t_1 = t_2$?
5. Find an expression of the group velocity $v(k)$ and effective mass $m^*(k)$ of both bands.
6. Derive an expression for the density of states $g(E)$ of the entire band structure and make a plot of it. Does your result makes sense when considering the band structure?

#### Exercise 3: atomic chain with 3 different spring constants
Suppose we have a vibrating 1D atomic chain with 3 different spring constants alternating like $\kappa_ 1$, $\kappa_2$, $\kappa_3$, $\kappa_1$, etc. All the the atoms in the chain have an equal mass $m$.

1. Make a sketch of this chain and indicate the length of the unit cell $a$ in this sketch.
2. Derive the equations of motion for this chain.
3. By filling in the trial solutions for the equations of motion (which should be similar to the ones in eqs. (10.3) and (10.4) of the book), show that the eigenvalue problem is given by $$ \omega^2 \mathbf{x} = \frac{1}{m} \begin{pmatrix} \kappa_1 + \kappa_ 3 &  -\kappa_ 1 & -\kappa_ 3 e^{i k a} \\ -\kappa_ 1 & \kappa_1+\kappa_2 & -\kappa_ 2 \\  -\kappa_ 3 e^{-i k a} & -\kappa_2 & \kappa_2 + \kappa_ 3 \end{pmatrix} \mathbf{x}$$
4. In general, the eigenvalue problem above cannot be solved analytically, and can only be solved in specific cases. Find the eigenvalues $\omega^2$ when $k a = \pi$ and $\kappa_ 1 = \kappa_ 2 = q$.

    ??? hint
        To solve the eigenvalue problem quickly, make use of the fact that the Hamiltonian in that case commutes with the matrix $$ X = \begin{pmatrix} 0 & 0 & 1 \\ 0 & 1 & 0 \\ 1 & 0 & 0 \end{pmatrix}. $$ What can be said about eigenvectors of two matrices that commute?

5. What will happen to the periodicity of the band structure if $\kappa_ 1 = \kappa_ 2 = \kappa_3$?