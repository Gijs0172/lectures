```{python initialize=true}
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Atoms and bonds
_(based on chapters 5 and 6.2 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe the shell-filling model of atoms
    - Derive the LCAO model
    - Obtain the spectrum of the LCAO model of several orbitals

## Looking back

So far we have:

* Introduced the $k$-space (reciprocal space)
* Postulated electron and phonon dispersion relations

As a result we:

* Understood how phonons store heat (Debye model)
* Understood how electrons conduct (Drude model) and store heat/energy (Sommerfeld model)

We used our best guess as a starting point, and there are several mysteries:

* Why is there a cutoff frequency? Why are there no more phonon modes?
* Why do electrons not scatter off from every single atom in the Drude model? Atoms are charged and should provide a lot of scattering.
* Why are some materials not metals? (Think if you know a crystal that isn't a metal)

## A quick review of atoms

### Why chemistry is not physics

**Everything** is described by the Schrödinger equation:

$$H\psi = E\psi,$$

with $H$ the sum of kinetic energy and Coulomb interaction, so for hydrogen we have:

$$H=-\hbar^2\frac{\partial^2}{2m\partial {\mathbf r^2}} - \frac{e^2}{4\pi\varepsilon_0|r|}$$

for helium it becomes more complex: $\psi({\mathbf r})\rightarrow \psi({\mathbf {r_1, r_2}})$, so

$$H=-\hbar^2\frac{\partial^2}{2m\partial {\mathbf r_1^2}} -\hbar^2\frac{\partial^2}{2m\partial {\mathbf r_2^2}}- \frac{2e^2}{4\pi\varepsilon_0|r_1|} - \frac{2e^2}{4\pi\varepsilon_0|r_2|} + \frac{e^2}{4\pi\varepsilon_0|r_1 - r_2|},$$

which means we need to find eigenvalues and eigenvectors of a 6-dimensional differential equation!

"Mundane" copper has 29 electrons, so to find the electronic spectrum of Copper we would need to solve an 87-dimensional Schrödinger equation, and there is no way in the world we can do so.

This exponential growth in complexity with the number of interacting quantum particles is why *many-body quantum physics* is very much an open area in solid state physics.

However we need to focus on what is possible to do, and apply heuristic rules based on the accumulated knowledge of how atoms work (hence we will need a bit of chemistry).

However for us the complexity of solving the problem is the reason why we need to accept empirical observations as *chemical laws* even though they work with limited precision, and are "merely" consequences of the Schrödinger equation.

### Quantum numbers and shell filling

Single electron states have 4 quantum numbers: $|n, l, l_z, s\rangle$

![](figures/single_atom.svg)

Quantum numbers:

* $n=1,2,\ldots$ is the azimuthal (principal) quantum number
* $l=0, 1, \ldots, n-1$ is the angular momentum (also known as $s, p, d, f$ orbitals)
* $l_z=-l, -l+1\ldots, l$ is the $z$-component of angular momentum
* $s$ is the spin

It turns out that electrons in all atoms approximately occupy very similar orbitals, only the energies are very different due to the Coulomb interaction.

The order of orbital filling is set by several rules:

* Aufbau principle: *first fill a complete shell (all electrons with the same $n, l$) before going to the next one*
* Madelung's rule: *first occupy the shells with the lowest $l+n$, and of those with equal $l+n$ those with smaller $n$*

Therefore shell-filling order is 1s, 2s, 2p, 3s, 3p, 4s, 3d, ...

The electrons in the outermost shell are the only ones participating in chemical reactions and electric conduction.
The rest provides a negatively charged cloud that reduces the attraction to the atomic nucleus.

## Covalent bonds and linear combination of atomic orbitals

*Here we present the material in the order different from the book because the covalent bonds are the most important for the exercises*

### Two atoms

Consider two atoms next to each other.

Since different orbitals of an atom are separated in energy, we consider one orbital per atom (even though this is often a bad starting point and it should only work for $s$-orbitals).

Let's imagine that the atoms are sufficiently far apart, so that the shape of the orbitals or the energy of the orbitals doesn't change much.

If $|1⟩$ is the wave function of an electron bound to the first atom, and $|2⟩$ is the wave function of the electron near the second atom, we will search for a solution in form: $|ψ⟩ = ϕ_{1}|1⟩ + ϕ_{2}|2⟩$, or in other words as a *Linear Combination of Atomic Orbitals (LCAO)*.

For simplicity let's assume now that $⟨1|2⟩=0$, so that $ψ$ is normalized whenever $|ϕ_1|^2 + |ϕ_2|^2 = 1$.
(See the book exercise 6.5 for relaxing this assumption)

Acting with the Hamiltonian on the LCAO wave function we get an eigenvalue problem:

$$
E \begin{pmatrix} ϕ_1 \\ ϕ_2 \end{pmatrix}
= \begin{pmatrix}
⟨1|H|1⟩ & ⟨1|H|2⟩ \\ ⟨2|H|1⟩ & ⟨2|H|2⟩
\end{pmatrix}
\begin{pmatrix} ϕ_1 \\ ϕ_2\end{pmatrix}
$$

It only depends on two parameters remaining of the original problem:
$⟨1|H|1⟩ = ⟨2|H|2⟩ ≡ E_0$ is the **onsite energy**, $⟨1|H|2⟩ ≡ -t$ is the **hopping integral** (or just **hopping**).

Since we are considering bound states, all orbitals $|n⟩$ are purely real ⇒ $t$ is real as well.

$$ H = \begin{pmatrix} E_0 & -t \\ -t & E_0 \end{pmatrix}$$

Eigenstates & eigenvalues:  
$|+⟩ = \tfrac{1}{\sqrt{2}}(|1⟩ + |2⟩)$ [even/symmetric superposition with $E_+ = E_0 - t$];  
$|-⟩ = \tfrac{1}{\sqrt{2}}(|1⟩ - |2⟩)$ [odd/antisymmetric superposition with $E_- = E_0 + t$].

Two atoms are a molecule, and $\psi_+$ and $\psi_-$ are **molecular orbitals**.

An even superposition has a lower kinetic energy (derivative is smaller) ⇒ $t$ is positive.

![](figures/two_atoms.svg)

### Bonding vs antibonding

Hopping $t$ grows when the atoms are moved together because the overlap. Let's plot energy of molecular orbitals vs inter-atomic distance.

![](figures/bonding.svg)

When $|+⟩$ is occupied by an electron (or two, because there are two states with opposite spin), it makes the atoms attract (or *bond*) because the total energy is lowered. It is therefore called a **bonding orbital**.

An occupied $|-⟩$ state increases the molecule energy as the atoms move closer ⇒ it makes atoms repel, and it is an **antibonding orbital**.

Therefore if each atom has a single electron in the outermost shell, these atoms attract, if there are 0 or 2 electrons, the net electron force cancels (but electrostatic repulsion remains).

### Summary

* Electrons in atoms occupy shells, with only the outermost shell contributing to interatomic interaction.
* The LCAO method reduces the full Hamiltonian to a finite size problem written in the basis of individual orbitals.
* If two atoms have one orbital and one atom each, electron movement makes the atoms attract.

*[LCAO]: Linear Combination of Atomic Orbitals

## Exercises

### Exercise 1: Shell-filling model of atoms
1. Describe the shell-filling model of atoms.
2. Use Madelung’s rule to deduce the atomic shellfilling configuration of the element tungsten, which has atomic number 74.
3. Although Madelung’s rule for the filling of electronic shells holds extremely well, there are a number of exceptions to the rule. Here are a few of them:
$$Cu = [Ar] 4s^1 3d^{10}$$
$$Pd = [Kr] 5s^0 4d^{10}$$
$$Ag = [Kr] 5s^1 4d^{10}$$
$$Au = [Xe] 6s^1 4f^{14} 5d^{10}$$
What should the electron configurations be if these elements followed Madelung’s rule and the Aufbau principle?

### Exercise 2: Application of the LCAO model

Consider an electron moving in 1D between two delta-function shaped potential wells.
The complete Hamiltonian of this one-dimensional system is then:
$$
H = \frac{p^2}{2m}-V_0\delta(x_1-x)-V_0\delta(x_2-x),
$$
with $V_0$ is the potential strength, $p$ the momentum of the electron, and $x_1$, $x_2$ the positions of the potential wells.

??? hint "Properties of a $\delta$-function potential"

    A delta function $\delta(x_0 - x)$ centered at $x_0$ is defined to be zero everywhere except for $x_0$, where it tends to infinity.
    Further, a delta function has the property:
    $$
    \int_{-\infty}^{\infty} f(x)\delta(x_0-x)dx = f(x_0).
    $$

    The procedure to find the energy and a wave function of a state bound in a $\delta$-function potential, $V=-V_0\delta(x-x_0)$, is similar to that of a quantum well:

    1. Assume that the bound state has the energy $E_0$.
    2. Compute the wave function $\phi$ in different regions of space: namely $x < x_0$ and $x > x_0$.
    3. Apply the boundary conditions at $x=x_0$. The wave function $\phi$ must be continuous, but $d\phi/dx$ is not. Instead due to the presence of the delta-function:
       $$
       \frac{d\phi}{dx}\Bigr|_{x_0+\epsilon} - \frac{d\phi}{dx}\Bigr|_{x_0-\epsilon}= -\frac{2mV_0}{\hbar^2}\phi(x_0).
       $$
    4. Find for which energy the boundary conditions at $x=x_0$ are satisfied. This is the energy of the bound state.
    5. Normalize the wave function.

Let us apply the LCAO model to solve this problem. Consider the trial wave function for the ground state to be a linear combination of two orbitals $|1\rangle$ and $|2\rangle$:
$$|Ψ ⟩ = ϕ_1|1⟩ + ϕ_2|2\rangle.$$
The orbitals $|1\rangle$ and $|2\rangle$ correspond to the wave functions of the electron when only one of the nuclei is present:    
$$H_1 |1\rangle = \epsilon_1 |1\rangle,$$
$$H_2 |2\rangle = \epsilon_2 |2\rangle,$$

1. Find the expressions for the wave functions of the states $|1⟩$ and $|2⟩$: $\psi_1(x)$ and $\psi_2(x)$, as well as their energies $ε_1$ and $ε_2$.
   Remember that you need to normalize the wave functions.
2. Find the LCAO Hamiltonian. To simplify the calculations, assume that the orbitals are orthogonal.  
3. Diagonalize the effective Hamiltonian. Which are the expressions for the eigenenergies of the system?

### Exercise 3: Polarization of a hydrogen molecule

Consider a hydrogen molecule as a one-dimensional system with two identical nuclei at $x=-\frac{d}{2}$ and $x=+\frac{d}{2}$, so that the center of the molecule is at $x=0$.

Consider two electrons in the system, each with charge $e$. The electrons are shared between the nuclei to form a covalent bond.
The LCAO Hamiltonian for each of the electrons is:
$$
H_{\textrm{eff}} = \begin{pmatrix}
          E_0&&-t \\
          -t&&E_0
          \end{pmatrix}.  
$$

1. Let us add an electric field $\mathcal{E}$ to the system. Which term needs to be added to the Hamiltonian of each electron?
2. Compute the LCAO Hamiltonian of the system in presence of the electric field.
   What are the new onsite energies of the two orbitals?
3. Diagonalize the modified LCAO Hamiltonian. Find the LCAO wave function of the ground state.
4. Find the polarization $P$ of the molecule in the ground state.

    ??? hint "Reminder: polarization"

        The polarization $P$ of a molecule with $n\leq 2$ electrons at its ground state $|\Psi\rangle$ is:
        $$
        P = n e \langle\Psi|x|\Psi\rangle.
        $$
        Use that ground state you found in 3.2 is a linear superposition of two orthogonal orbitals centered at $-\frac{d}{2}$ and $+\frac{d}{2}$.
