```{python initialize=true}
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

_(based on chapter 3 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - discuss the basics of Drude theory, which describes electron motion in metals.
    - use Drude theory to analyze transport of electrons through conductors in electric and magnetic fields.
    - describe central terms such as the mobility and the Hall resistance.

### Drude theory
Ohm's law states that $V=IR=I\rho\frac{l}{A}$. In this lecture we will investigate where this law comes from. We will use the theory developed by Paul Drude in 1900, which is based on three assumptions:

- Electrons have an average scattering time $\tau$.
- At each scattering event an electron returns to momentum ${\bf p}=0$.
- In-between scattering events electrons respond to the Lorentz force ${\bf F}_{\rm L}=-e\left({\bf E}+{\bf v}\times{\bf B}\right)$.

```python
import numpy as np
import matplotlib.pyplot as plt

walker_number = 20 # number of particles
tau = 1 # relaxation time
gamma = .3 # dissipation strength
a = 1 # acceleration
dt = .1 # infinitesimal
T = 20 # simulation time 

v = np.zeros((2, int(T // dt), walker_number), dtype=float) 

scattering_events = np.random.binomial(1, dt/tau, size=v.shape[1:])
angles = np.random.uniform(high=2*np.pi, size=scattering_events.shape) * scattering_events
rotations = np.array(
    [[np.cos(angles), np.sin(angles)],
     [-np.sin(angles), np.cos(angles)]]
)

for step in range(1, v.shape[1]):
    v[:, step] = v[:, step-1]
    v[0, step] += a * dt
    v[:, step] = np.einsum(
        'ijk,jk->ik',
        rotations[:, :, step-1, :],
        v[:, step, :]
    ) * (1 - gamma * scattering_events[step-1])

r = np.cumsum(v * dt, axis=1)

scattering_positions = np.copy(r)
scattering_positions[:, ~scattering_events.astype(bool)] = np.nan

plt.plot(*r[:, :100], alpha=.5, c='#1f77b4');
plt.scatter(*scattering_positions[:, :100], s=10);
plt.axis('off');
```

We start by considering only an electric field (_i.e._ ${\bf B}=0$). What velocity do electrons acquire in-between collisions?

$$
{\bf v}=-\int_0^\tau\frac{e{\bf E}}{m_{\rm e}}{\rm d}t=-\frac{e\tau}{m_{\rm e}}{\bf E}=-\mu{\bf E},
$$

where we have defined the _mobility_ $\mu\equiv e\tau/m_{\rm e}$. The current density ${\bf j}$ [A/m$^2$] is given by:

$$
{\bf j}=-en{\bf v}=\frac{n e^2\tau}{m_{\rm e}}{\bf E}=\sigma{\bf E}\ ,\ \ \sigma=\frac{ne^2\tau}{m_{\rm e}}=ne\mu
$$

where $n$ is the density of electrons, and $\sigma$ is the conductivity, which is the inverse of resistivity $\rho=\frac{1}{\sigma}$. 

If we now take $j=\frac{I}{A}$ and $E=\frac{V}{l}$, we retrieve Ohm's Law: $\frac{I}{A}=\frac{V}{\rho l}$.

Scattering is caused by collisions with:

- Phonons: $\tau_{\rm ph}(T)$ ($\tau_{\rm ph}\rightarrow\infty$ as $T\rightarrow 0$)
- Impurities/vacancies: $\tau_0$

Scattering rate $\frac{1}{\tau}$:

$$
\frac{1}{\tau}=\frac{1}{\tau_{\rm ph}(T)}+\frac{1}{\tau_0}\ \Rightarrow\ \rho=\frac{1}{\sigma}=\frac{m}{ne^2}\left( \frac{1}{\tau_{\rm ph}(T)}+\frac{1}{\tau_0} \right)\equiv \rho_{\rm ph}(T)+\rho_0
$$

![](figures/matthiessen.svg)

_Matthiessen's Rule_ (1864). Solid (dashed) curve: $\rho(T)$ for a pure (impure) crystal.

How fast do electrons travel through a copper wire? Let's take $E$ = 1 volt/m, $\tau$ ~ 25 fs (Cu, $T=$ 300 K).

$\rightarrow v=\mu E=\frac{e\tau}{m_{\rm e}}E=2.5$ mm/s ! (= 50 $\mu$m @ 50 Hz AC)

### Hall effect
We now consider a conductive wire in a magnetic field ${\bf B} \rightarrow$ electrons are deflected in a direction perpendicular to ${\bf B}$ and ${\bf j}$ by the Lorentz force.

![](figures/hall_effect.svg)

${\bf E}_{\rm H}$ is the electric field caused by the Lorentz force, leading to a _Hall voltage_ in the direction perpendicular to ${\bf B}$ and ${\bf j}$.

In steady state, there is no current flow in the $y$-direction because the $y$-component of the Lorentz force $-e{\bf v}_x\times{\bf B}$ is being compensated by the Hall electric field ${\bf E}_{\rm H}={\bf v}_x\times{\bf B}=\frac{1}{ne}{\bf j}\times{\bf B}$. The total electric field then becomes:

$$
{\bf E}=\left(\frac{1}{ne}{\bf j}\times{\bf B}+\frac{m}{ne^2\tau}{\bf j}\right).
$$
where the second term is associated with the Drude resistivity derived above. 

We now introduce the _resistivity matrix_ $\tilde{\rho}$ as ${\bf E}=\tilde{\rho}{\bf j}$, where the diagonal elements are simply $\rho_{xx}=\rho_{yy}=\rho_{zz}=\frac{m}{ne^2\tau}$. The off-diagonal element $\rho_{xy}$ gives us:

$$
\rho_{xy}=\frac{B}{ne}\equiv -R_{\rm H}B
$$

where $R_{\rm H}=-\frac{1}{ne}$ is the *Hall coefficient*. So by measuring the Hall coefficient, we can obtain $n$, the density of free electrons in a material.

While most materials have $R_{\rm H}<0$, interestingly some materials are found to have $R_{\rm H}>0$. This would imply that the charge of the carriers is positive. We will see later (chapter 17) how to interpret this.
## Conclusions

  1. Drude theory leads to Ohm's law. Resistivity is caused by electrons that scatter with some characteristic time $\tau$.
  2. The Lorentz force leads to a 'Hall voltage' that is perpendicular to the direction of electric current pushed through a material.
 

## Exercises
### Exercise 1: Extracting quantities from basic Hall measurements
We apply a magnetic field $\bf B$ perpendicular to a planar (two-dimensional) sample that sits in the $xy$ plane. The sample has width $W$ in the $y$-direction, length $L$ in the $x$-direction and we apply a current $I$ along $x$.

  1. Suppose we measure a Hall voltage $V_H$. Express the Hall resistance $R_{xy} = V_H/I$ as a function of magnetic field. Does $R_{xy}$ depend on the geometry of the sample? Also express $R_{xy}$ in terms of the Hall coefficient $R_H$.
  2. Assuming we know the charge density $n$ in the sample, what quantity can we extract from a measurement of the Hall resistance? Would a large or a small electron density give a Hall voltage that is easier to measure? 
  3. Express the longitudinal resistance $R=V/I$, where $V$ is the voltage difference over the sample along the $x$ direction, in terms of the longitudinal resistivity $\rho_{xx}$. Suppose we extracted $n$ from a measurement of the Hall resistance, what quantity can we extract from a measurement of the longitudinal resistance? Does the result depend on the geometry of the sample?

### Exercise 2: Motion of an electron in a magnetic and an electric field.
We first consider an electron in free space, moving in a plane perpendicular to a magnetic field ${\bf B}$ with velocity ${\bf v}$.

  1. What is the shape of the motion of the electron? Calculate the characteristic frequency and time-period $T_c$ of this motion for $B=1$ Tesla.
  2. Write down the Newton's equation of motion for the electron, compute $\frac{d\mathbf{v}}{{dt}}$.
  3. Now we accelerate the electron by adding an electric $\mathbf{E}$ that is perpendicular to ${\bf B}$. Sketch the motion of the electron.
  4. Adjust the differential equation for $\frac{d\mathbf{v}}{{dt}}$ found in (2) to include ${\bf E}$.
  5. We now consider an electron in a metal. Include the Drude scattering time $\tau$ into the differential equation for the velocity you formulated in 4.


### Exercise 3: Temperature dependence of resistance in the Drude model
   We consider copper, which has a density of 8960 kg/m$^3$, an atomic weight of 63.55 g/mol, and a room-temperature resistivity of $\rho=1.68\cdot 10^{-8}$ $\Omega$m. Each copper atom provides one free electron.
  
  1. Calculate the Drude scattering time $\tau$ at room temperature.
  2. Assuming that electrons move with the thermal velocity $\langle v \rangle = \sqrt{\frac{8k_BT}{\pi m}}$, calculate the electron mean free path $\lambda$.
  3. The Drude model assumes that $\lambda$ is independent of temperature. How does the electrical resistivity $\rho$ depend on temperature under this assumption? Sketch $\rho(T)$.
  5. Compare your sketch of $\rho(T)$ with that in the lecture notes. In what respect do they differ? Discuss possible reasons for differences.

### Exercise 4: The Hall conductivity matrix and the Hall coefficient
We apply a magnetic field $\bf B$ perpendicular to a current carrying 2D sample in the xy plane. In this situation, the electric field $\mathbf{E}$ is related to the current density $\mathbf{J}$ by the resistivity matrix:

$$\mathbf{E} = \begin{pmatrix} \rho_{xx} & \rho_{xy} \\ \rho_{yx} & \rho_{yy} \end{pmatrix} \mathbf{J}$$
 
  1. Sketch $\rho_{xx}$ and $\rho_{xy}$ as a function of the magnetic field $\bf B$.
  2. Invert the resistivity matrix to obtain the conductivity matrix $$\begin{pmatrix} \sigma_{xx} & \sigma_{xy} \\ \sigma_{yx} & \sigma_{yy} \end{pmatrix} $$, allowing you to express $\mathbf{J}$ as a function of $\mathbf{E}$. 
  3. Sketch $\sigma_{xx}$ and $\sigma_{xy}$ as a function of the magnetic field $\bf B$. 
  4. Give the definition of the Hall coefficient. What does the sign of the Hall coefficient indicate?
  
