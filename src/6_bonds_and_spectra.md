```{python initialize=true}
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# interatomic interaction and molecular spectra
_(based on chapters 6.1, 6.3, 8 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Explain the origins of interatomic forces
    - Compute vibrational spectra of small molecules in 1D
    - Formulate Hamiltonians and equations of motion of bulk materials (but not yet solve them)

## Adding repulsion

If bringing two atoms closer would keep increasing energy, any covalent bond would collapse; eventually the two atoms must start repelling (at least when the nuclei get close, but really already much earlier).

![](figures/bonding_with_repulsion.svg)

### Van der Waals bond

*Not our focus, but we will discuss it for completeness*

Originates from attraction between dipole moments of two atoms:

If one atom has a dipole moment $p_1$, it creates electric field

$$ E = \frac{p_1}{4\pi \varepsilon_0 r^3} $$

at the position of another atom. The other atom then develops a dipole moment $p_2 = \chi E$ with $\chi$ the *polarizability* of the atom.

This results in an attractive potential $U(r) = -E p_2 \sim r^{-6}$.

This attraction is much weaker than covalent bonds but drops slower with increasing distance. (Q: how fast does the interaction of covalent bonds drop?)

Van der Waals bonds hold layers of covalently bonded carbon atoms together when forming graphite:

![Graphite atomic layers](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Graphite-layers-side-3D-balls.png/320px-Graphite-layers-side-3D-balls.png)  
(image source: Wikipedia)

## First steps towards phonons

The atomic interaction is minimized when the distance between the atoms is at the minimum of the potential: $U = U_0$ when $\delta r = a$.

![](figures/interatomic_interaction.svg)

Near the minimum, the potential is approximately parabolic:

$$
U = U_0 + \frac{\kappa}{2} (\delta r - a)^2 - \frac{\kappa_3}{3!} (\delta r - a)^3 + \ldots
$$

This is a harmonic oscillator potential with the higher order term $\kappa_3$ providing anharmonicity.

### Rigidity

If we stretch a material with length $L$ containing $N=L/a$ atoms by a lenght $\delta L$, the displacement of each atom is $\delta r - a = \delta L/N$.

The returning force is

$$
F = - \frac{d U}{d(\delta r)} (\delta r - a) = \kappa a \delta L / L,
$$

which leads us to the material *compressibility*

$$
\beta \equiv -\frac{1}{L} \frac{\partial L}{\partial F} = \frac{1}{\kappa a}.
$$

Then using compressibility we can derive the wave equation and obtain the speed of sound in the material relating it to the bond strength.

### Thermal expansion

The nonlinearity $\kappa_3$ means that the potential grows slower when the atoms are further apart than when they are closer to each other.

Therefore the larger the energy of the atoms, the more their average distance increases $\Rightarrow$ we have a model of thermal expansion.

## Looking ahead: multiple atoms

Our aim is to understand electrons and phonons in solids containing $N\to\infty$ atoms.

We now have a microscopic model of what happens when there are two atoms; let's try to see what happens when we take several.

### Phonons

Our plan:

* Consider only *harmonic potential* acting between atoms
* Write down equations of motion
* Compute normal modes

For simplicity we consider 1D motion, and let's start with 3 atoms:

![](figures/phonons2.svg)

Newton's equations of motion:

$$
\begin{aligned}
m \ddot{x}_1 &= - \kappa (x_1 - x_2) \\
m \ddot{x}_2 &= -\kappa (x_2 - x_1) -\kappa (x_2 - x_3) \\
m \ddot{x}_3 &= - \kappa (x_3 - x_2)
\end{aligned},
$$

Or in matrix form:

$$
m \ddot{x} = -\kappa
\begin{pmatrix}
1 & -1 & 0\\
-1 & 2 & -1\\
0 & -1 & 1
\end{pmatrix}x
$$

We search for *normal modes*: patterns of motion that are periodic and have a fixed frequency: $x(t) = x_0 e^{i\omega t}$.

Substituting into the equations of motion we get an eigenvalue problem:

$$
\omega^2 x_0 = \frac{\kappa}{m}
\begin{pmatrix}
1 & -1 & 0\\
-1 & 2 & -1\\
0 & -1 & 1
\end{pmatrix}x_0.
$$

The solutions of this eigenvalue problem are the phonon modes that we occupy.

### Electrons

Same as a single covalent bond, only more atoms in a line. Considering 3 atoms:

$$
E \begin{pmatrix}
c_1 \\ c_2 \\ c_3
\end{pmatrix} =
\begin{pmatrix}
E_0 & t & 0 \\
t & E_0 & t \\
0 & t & E_0
\end{pmatrix}
\begin{pmatrix}
c_1 \\ c_2 \\ c_3
\end{pmatrix}
$$

### Numerical test

Diagonalizing large matrices is unwieldy, but let's try and check it numerically to see if we notice a trend.

Eigenfrequencies of 3 atoms: `[0.0 1.0 1.732050]`

```python
def DOS_finite_phonon_chain(n):
    rhs = 2 * np.eye(n) - np.eye(n, k=1) - np.eye(n, k=-1)
    rhs[0, 0] -= 1
    rhs[-1, -1] -= 1
    pyplot.figure()
    pyplot.hist(np.sqrt(np.abs(np.linalg.eigvalsh(rhs))), bins=30)
    pyplot.xlabel("$\omega$")
    pyplot.ylabel("Number of levels")

DOS_finite_phonon_chain(3)
```

Energies of 3 orbitals: `[-1.41421356 0.0  1.41421356]`

```python
def DOS_finite_electron_chain(n):
    rhs = - np.eye(n, k=1) - np.eye(n, k=-1)
    pyplot.figure()
    pyplot.hist(np.linalg.eigvalsh(rhs), bins=30)
    pyplot.xlabel("$E$")
    pyplot.ylabel("Number of levels")

DOS_finite_electron_chain(3)
```

### From 3 atoms to 300

Frequencies of many phonons:

```python
DOS_finite_phonon_chain(300)
```

Energies of many electrons:

```python
DOS_finite_electron_chain(300)
```
## Summary

* Electrons in atoms occupy "shells" in the energetically favorable order
* When two atoms come close, electrons occupy molecular orbitals and bind atoms together.
* Motion of electrons makes atoms attract
* Oscillatory motion of atoms and hopping of electrons between atoms give rise to the macroscopic behavior of the materials (next week)

### Exercises

## Exercise 1: Linear triatomic molecule

Consider carbon dioxide (C0$_2$) which is a linear triatomic molecule shown below

<img src="https://upload.wikimedia.org/wikipedia/commons/a/af/Carbon-dioxide-3D-vdW.svg" width="50%" alt="carbon dioxide"></img>

??? info "source"

    By Jasek FH. - Own work, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/3.0 "Creative Commons Attribution-Share Alike 3.0"), [Link](https://commons.wikimedia.org/w/index.php?curid=2875238)


1. How many normal modes does this molecule have assuming motion in only 1D? How many normal modes does it have if the atoms can move in all three dimensions?
2. For simplicity, we only consider 1D motion of the atoms. Write down Newton's equations of motion for the atoms.
3. Consider a *symmetric* mode, for which the displacements of the oxygen atoms are equal in magnitude and have an opposite direction. Find the eigenfrequency of this mode.
4. Now consider the antisymmetric mode when the oxygen atoms move in phase and have the same displacement. Find the ratio between the displacements of the carbon and oxygen atoms that keeps the center of mass at rest.
5. Compute the eigenfrequency of the antisymmetric mode. 

??? hint

    Compare your answers with [Wikipedia](https://en.wikipedia.org/wiki/Triatomic_molecule).

## Exercise 2: Lennard-Jones potential

A simple model approximating the interaction between a pair of noble gas atoms such as Argon is the [Lennard-Jones potential](https://en.wikipedia.org/wiki/Lennard-Jones_potential), in which the potential energy as a function of interatomic distance is

$$ U(r) = 4\epsilon\big[\big(\frac{\sigma}{r}\big)^{12}-\big(\frac{\sigma}{r}\big)^6\big] $$

where $r$ is the distance between two atoms, $\epsilon$ is the depth of the potential well, and $\sigma$ is the distance at which the inter-particle potential is zero.

1. Sketch $U(r)$ as a function of interatomic distance and mark the regions of repulsive and attractive forces acting between the atoms.
2. Find the distance, $r_0$ (bond length), at which the potential energy is minimal and find the value of the potential energy at this distance (binding energy of the molecule).
3. Expand $U(r)$ in a Taylor series around $r_0$ up to second order. By considering a second-order (=harmonic) potential approximation around the minimum ($r_0$), find an expression for the spring constant, $K$, in terms of $\epsilon$ and $\sigma$.
4. In the harmonic potential approximation of the Lennard-Jones potential for Argon, find the ground-state energy of the molecule. What is the energy required to break the molecule apart?
5. What is the approximate number of phonons that can occupy this mode before the potential becomes anharmonic?

## Exercise 3: Numerical simulation

For this exercise, use Matlab (or any other programming environment you are comfortable with).

1. Define a matrix that relates forces to displacements in a linear 1D chain containing $N=5$ atoms. Repeat for $N=200$.

    ??? hint

        In Matlab use the function [`diag`](https://nl.mathworks.com/help/matlab/ref/diag.html).

2. Using numerical diagonalization ([`eig`](https://nl.mathworks.com/help/matlab/ref/eig.html)), compute the eigenfrequencies of this atomic chain. Plot a histogram of these eigenfrequencies.
3. Make the masses of every even atom different from the masses of every odd atom. Compute the eigenfrequencies of this atomic chain and plot a histogram.
