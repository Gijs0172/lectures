jupyter_client
ipython
ipykernel
pathlib
nbconvert
plotly
matplotlib
scipy
wikitables
pandas
notedown
python-markdown-math
mkdocs-material
